package org.gcube.portal.custom.communitymanager;

public class ThemesIdManager {
	/**
	 * 
	 */
	public static final String GCUBE_PUBLIC_THEME = "gcubepublictheme";
	/**
	 * 
	 */
	public static final String GCUBE_LOGGEDIN_THEME = "gcubeportaltheme";
	/**
	 * 
	 */
	public static final String iMARINE_PUBLIC_THEME = "imarinepublictheme";
	/**
	 * 
	 */
	public static final String iMARINE_LOGGEDIN_THEME = "imarineportaltheme";
	
}
