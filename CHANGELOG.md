
# Changelog for Custom Portal Handler

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v2.1.1] - 2023-03-30

- ported to git

## [v1.8.0] - 2015-10-22

- Added method for retrieving Virtual Groups

## [v1.0.0] - 2012-05-04

- First release
